package cl.qlo.dev.demesurut;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import cl.qlo.dev.demesurut.rut.RutDV;

@EActivity(R.layout.activity_force_rut)
public class ForceRut extends AppCompatActivity {

    @ViewById(R.id.input_rut)
    EditText inputRut;

    @ViewById(R.id.rut_gen)
    TextView genRut;

    @AfterViews
    void showKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @TextChange(R.id.input_rut)
    void update() {
        String input = inputRut.getText().toString();
        if(!input.equals("")) {
            try{
                int rutin = Integer.parseInt(input); // MAX = 2,147,483,647
                RutDV newRut = new RutDV(rutin);
                genRut.setText(newRut.toString());
            }
            catch(NumberFormatException e) {
                Toast.makeText(getApplicationContext(),"El número ingresado es muy grande", Toast.LENGTH_LONG).show();
            }
        } else {
            genRut.setText("");
        }
    }
}
