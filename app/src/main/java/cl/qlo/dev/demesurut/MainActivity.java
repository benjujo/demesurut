package cl.qlo.dev.demesurut;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @Click(R.id.randomRut)
    void randomRut() {
        Intent i = new Intent(this, RandomRut_.class);
        startActivity(i);
    }

    @Click(R.id.forceRut)
    void forceRut() {
        Intent i = new Intent(this, ForceRut_.class);
        startActivity(i);
    }
}
