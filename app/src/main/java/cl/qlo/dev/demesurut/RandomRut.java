package cl.qlo.dev.demesurut;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cl.qlo.dev.demesurut.rut.RandomInt;
import cl.qlo.dev.demesurut.rut.RutDV;

@EActivity(R.layout.activity_random_rut)
public class RandomRut extends AppCompatActivity {

    @ViewById(R.id.rut)
    TextView rut;

    @Click(R.id.another)
    @AfterViews
    public void newRut() {
        int rdnRut = RandomInt.getInt();
        RutDV newRut = new RutDV(rdnRut);
        rut.setText(newRut.toString());
    }

}
