package cl.qlo.dev.demesurut.rut;

import java.text.NumberFormat;
import java.util.Locale;

public class RutDV {
    private int rut;

    public RutDV(int rut) {
        this.rut = rut;
    }

    private char dv() {
        int rut = this.rut;
        int cnt = 2;
        int acc = 0;
        int dv;

        while(rut>0) {
            acc += (rut%10)*cnt;
            rut /= 10;
            cnt++;
            if(cnt>7)
                cnt = 2;
        }
        dv = (11 - acc%11)%11;
        if(dv == 10) {
            return 'K';
        }
        return (char) (48 + dv);
    }

    private String formatInt() {
        Locale cl = new Locale("es", "CL");
        NumberFormat nf = NumberFormat.getInstance(cl);
        return nf.format(this.rut);
    }

    public String toString() {
        return formatInt() + "-" + dv();
    }
}
